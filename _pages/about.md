---
layout: about
title: about
permalink: /
subtitle: rahul <b>[at]</b> ucsd <b>[dot]</b> edu <br> Assistant Professor of <a href="https://www.ece.ucsd.edu/">ECE<a> at <a href="https://ucsd.edu/">UCSD</a> 

profile:
  align: right
  image: rahul/rahul-epfl-bm-2023.jpg
  image_circular: false # crops the image to make it circular
  more_info: >
    <p>6406 Jacobs Hall</p>
    <p>9500 Gilman Dr, MC 0407</p>
    <p>La Jolla, CA 92093</p>

news: true # includes a list of news items
selected_papers: true # includes a list of papers marked as "selected={true}"
social: true # includes social icons at the bottom of the page
---

I am an assistant professor of electrical and computer engineering
([ECE](https://www.ece.ucsd.edu/)) at the University of California, San Diego
([UCSD](https://ucsd.edu/)), which I joined in 2024.  Previously, I was a
postdoctoral researcher at the École Polytechnique Fédérale de Lausanne
([EPFL](https://www.epfl.ch/en/)) from 2022 to 2024, where I worked with
[Michael Unser](http://bigwww.epfl.ch/unser/). I obtained my Ph.D. in 2022 at
the University of Wisconsin&ndash;Madison
([UW&ndash;Madison](https://www.wisc.edu/)), where I was supervised by [Robert
D. Nowak](https://nowak.ece.wisc.edu/). I completed my undergraduate studies at
the University of Minnesota, Twin Cities ([UMN](https://twin-cities.umn.edu/))
in 2018.

**&#9733; I am actively recruiting Ph.D. students to begin in Fall 2025 as well
as postdocs who are generally interested in the mathematics of data science. If
you are interested in working with me, please send me an email with (i) your CV
and (ii) a list of topics you are interested in.**

My research lies at the interface between functional/harmonic analysis, signal
processing, machine learning, and nonparametric statistics. I am primarily
interested in the **mathematics of data science** with a particular focus on the
foundations of **neural networks** and **deep learning**. Some questions my
research aims to answer include:

1. What is the effect of regularization in deep learning?
2. What kinds of functions do neural networks learn?
3. Why do neural networks seemingly break the curse of dimensionality?

Other topics/keywords that catch my attention include
**computed tomography**,
**geometry of Banach spaces**,
**inverse problems**,
**sparsity**,
**splines**,
**time--frequency analysis**, and
**wavelets**.
For more information, you can take a look at my [papers](/papers/).

